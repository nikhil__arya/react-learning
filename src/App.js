import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import logo from './logo.svg';
import './App.css';



class App extends Component {
  render() {
    //const myName = "Nikhil Arya"
    return (
      <div className="App">
        <h2>Hello my number is:</h2>
        <Body />

      </div>
    );
  }
}

class Body extends Component {

  constructor(props){
    super(props);

    this.state = {
      r: 0
    };
    this.getRandomNumber = this.getRandomNumber.bind(this);
  }

  getRandomNumber() {
    //console.log("button is clicked")
    this.setState({
      r: Math.floor(Math.random()*10)
    });
  }

  render(){
    return(
      <div>
        <button className="btn btn-primary" onClick={this.getRandomNumber}>Click Me!</button>
        <br />
        <Number myNumber={this.state.r}/>
      </div>
    );
  }
}

class Number extends Component {
  render(){
    return(
      <div>
          {this.props.myNumber}
      </div>
    );
  }
}

App.propTypes = {
  propObject: PropTypes.object,
  probString: PropTypes.string,
  propNumber: PropTypes.number
}

App.defaultProps = {
  propNumber: 5
}

class Parent extends Component {

  constructor(props){
    super(props);

    this.state = {
      cars: ["s-BMW", "s-Audi", "s-Bugati", "s-Aston"]
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      cars: this.state.cars.reverse()
    });
  }

  render(){
    return(
      <div>
        <h3 onClick={this.handleClick}>I am from Parent Component</h3>
        <Child msg="This is a message" coolcars={this.state.cars}/>
      </div>
    );
  }
}
Parent.defaultProps = {
  cars: ["BMW", "Audi", "Bugati"]
}

class Child extends Component {
  render(){
    return(
      <div>
        <h3>I am from Child Component</h3>
        <div>{this.props.coolcars.map((item, i) => {
            return <p key={i}>{item}</p>
          })}</div>
      </div>
    );
  }
}




export default App;
